# movie_app - aplikacja do wyszukiwania filmów

Aplikacja jest dosępna tutaj : https://jolantakazmierczak.github.io/

Aplikacja została utworzona przy użyciu frameworka Vue.js w wersji 2.
Dodatkowe paczki jakie zostały użyte w aplikacji to m.in. :
- axios
- vue-fontawesome
- free-solid-svg-icons
- eslint
- prettier

## Instrukcja instalacji

```
1. należy wykonać polecenie `git clone git@gitlab.com:jolanta.kazmierczak99/movie_app.git`
2. należy wykonać polecenie `cd movie_app`
2. należy wygenerować api-key na stronie : https://developers.themoviedb.org/3/getting-started/introduction
3. api-key należy przypisać do klucza VUE_APP_API_KEY w pliku .env
4. należy wykonać polecenie : `npm install`
5. należy wykonać polecenie : `npm run serve` 
6. aplikacja powinna być dostępna pod adresem: http://localhost:8080/ 
```