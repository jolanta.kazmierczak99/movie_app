interface MovieDetails {
  id: number;
  imdb_id: string;
  genres: Array<{ id: number; name: string }>;
  production_countries: Array<{ iso_3166_1: string; name: string }>;
}
export default MovieDetails;
