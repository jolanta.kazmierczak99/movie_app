import Movie from "@/model/movie/Movie";

interface MovieList {
  page: number;
  total_pages: number;
  total_results: number;
  results: Movie[];
}

export default MovieList;
