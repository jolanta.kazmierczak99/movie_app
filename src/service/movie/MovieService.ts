import Movie from "@/model/movie/Movie";
import Vue from "vue";
import MovieList from "@/model/movie/MovieList";

class MovieService {
  config = {
    headers: {
      Origin: "https://api.themoviedb.org",
    },
  };

  async getList(query: string, page: number): Promise<MovieList> {
    const { data } = await Vue.axios.get(
      `${process.env.VUE_APP_API_URL}/3/search/movie/?api_key=${process.env.VUE_APP_API_KEY}&query=${query}&page=${page}`,
      this.config
    );
    return data;
  }

  async loadDetails(movie: Movie): Promise<Movie> {
    const { data } = await Vue.axios.get(
      `${process.env.VUE_APP_API_URL}/3/movie/${movie.id}?api_key=${process.env.VUE_APP_API_KEY}`,
      this.config
    );

    movie.details = data;

    return movie;
  }
}

export default new MovieService();
